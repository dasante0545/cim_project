<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ConfirmPasswordController;


//Auth::routes(['login' => false, 'register' => false]);
// Authentication Routes...
Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'login']);
Route::any('logout', [LoginController::class, 'logout'])->name('logout');

// Registration Routes...
Route::get('register', [RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('register', [RegisterController::class, 'register']);

// Password Reset Routes...
Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
Route::post('password/reset', [ResetPasswordController::class, 'reset'])->name('password.update');

// Confirm Password Routes...
Route::get('password/confirm', [ConfirmPasswordController::class, 'showConfirmForm'])->name('password.confirm');
Route::post('password/confirm', [ConfirmPasswordController::class, 'confirm']);



Route::get('/', function () { return redirect()->route('login');});
Route::middleware(['auth'])->group( function(){

    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::resource('members', MemberController::class)->except(['destroy']);
    Route::get('members/{member}/destroy', [MemberController::class, 'destroy'])->name('members.destroy');
    Route::get('members/{member}/events-attended', [MemberController::class, 'EventsAttended'])->name('members.events.attended');

    Route::resource('event_categories', EventCategoryController::class)->except(['destroy']);
    Route::get('event_categories/{event_category}/destroy', [EventCategoryController::class, 'destroy'])->name('event_categories.destroy');

    Route::resource('events', EventController::class)->except(['destroy']);
    Route::get('events/{event}/destroy', [EventController::class, 'destroy'])->name('events.destroy');
    Route::any('events/add/members-to-event/{event}', [EventController::class, 'AddMembersToEvent'])->name('add.members.to.event');

    Route::resource('departments', DepartmentController::class)->except(['destroy']);
    Route::get('departments/{department}/destroy', [DepartmentController::class, 'destroy'])->name('departments.destroy');

    Route::resource('groups', GroupController::class)->except(['destroy']);
    Route::get('groups/{group}/destroy', [GroupController::class, 'destroy'])->name('groups.destroy');

});
