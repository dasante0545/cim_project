
@if(Session::has('error') || Session::has('success'))

    @if(Session::has('success'))

        <div class="alert alert-success alert-shadow alert-dismissible fade show" role="alert">
            <span class="alert-inner--icon"><i class="fas fa-check"></i></span>
            <span class="alert-inner--text"><strong>Success!</strong> {{ Session::get('success') }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

    @elseif(Session::has('error'))
        <div class="alert alert-danger alert-shadow alert-dismissible fade show" role="alert">
            <span class="alert-inner--icon"><i class="fas fa-times"></i></span>
            <span class="alert-inner--text"><strong>Danger</strong> {{ Session::get('error') }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

@endif
