@extends('layouts.admin')

@section('title', 'Departments')
@section('admin_css')

    <link rel="stylesheet" href="{!! asset('css/dataTables.bootstrap.min.css') !!}" type="text/css">
    <link rel="stylesheet" href="{!! asset('css/dataTables.bootstrap4.css') !!}" type="text/css">

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css"/>

@endsection
@section('content')


    <div class="">

        <div class="card">
            <div class="card-header">
                Departments

                <div class="float-right" data-toggle="modal" data-target="#modal_1">
                    <a class="btn btn-tertiary text-white btn-sm"><i class="fa fa-plus-circle"></i> Add Department</a>
                </div>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                @include('includes.alert')
                <!--begin: Datatable-->
                {!! $dataTable->table(['class'=> 'table table-striped table-bordered',]) !!}
                <!--end: Datatable-->
                </div>
            </div>
        </div>

    </div>






    <!-- Create Event Category Modal -->
    <div class="modal fade position-fixed" id="modal_1" tabindex="-1" role="dialog" aria-labelledby="modal_1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_title_6">Add Department</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('departments.store') }}">
                    @csrf
                    <div class="modal-body">
                        <div class="row col-md-12">

                            <div class="form-group col-md-12">
                                <label>Department Name</label>
                                <input type="text" name="department" value="{{ old('department') }}" class="form-control @error('department') is-invalid @enderror" required placeholder="">
                                @error('department')
                                <span class="text-danger" role="alert">
                               {{ $message }}
                           </span>
                                @enderror
                            </div>


                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-primary">Save & Apply</button>
                        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="{!! asset('vendor/datatables/buttons.server-side.js') !!}"></script>
    <script src="{!! asset('js/dataTables.buttons.min.js') !!}"></script>
    <script src="{!! asset('js/buttons.server-side.js') !!}"></script>
    {!! $dataTable->scripts() !!}

@endpush



