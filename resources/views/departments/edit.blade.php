@extends('layouts.admin')

@section('title', 'Departments')
@section('content')

    <div class="row">
        <div class="col-lg-5 mx-auto">
            <div class="card">
                <div class="card-header">
                    Edit Department
                </div>
                <div class="card-body">
                    <form method="POST" action="{{route('departments.update', $department->id)}}">
                        @method('PATCH')
                        @csrf
                        <div class="form-group">
                            <label>Department Name</label>
                            <input type="text" name="department" value="{{ old('department') ?? $department->department }}" class="form-control @error('department') is-invalid @enderror">
                            @error('department')
                            <span class="text-danger" role="alert">
                       {{ $message }}
                   </span>
                            @enderror
                        </div>
                        <div class="text-center">
                            <a href="{{ route('departments.index') }}" class="btn btn-sm btn-danger  text-white mr-2"> <i class="fa fa-sm fa-arrow-left"></i> Cancel</a>

                            <button type="submit" class="btn btn-sm btn-primary"> Update </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection
