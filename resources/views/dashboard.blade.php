@extends('layouts.admin')



@section('content')

  <div class="content-inner content-docs">
      <div class="row">
        <div class="col-lg ">
          <div class="card card-pricing popular text-center px-3 mb-4 z-depth-3">
            <div class="card-body">
              <div class="row">
                <div class="col-lg">
                  <img src="{{ asset('public/images/1.png') }}" alt="{{ asset('public/images/one.png') }}" class="img-fluid" width="90">

                </div>
                <div class="col-lg text-center">
                  <h5 class="text-dark">Total <br> Members</h5> <br>
                  <h3 class="text-center text-dark"> {{ $members }}</h3>
                </div>
              </div>
            </div>
          </div>
        </div>

{{--          style="border:3px solid #00c759 !important;"--}}
{{--          style="border:3px solid #171a1d !important;"--}}

        <div class="col-lg">
          <div class="card card-pricing popular text-center px-3 mb-4 z-depth-3" >
            <div class="card-body">
              <div class="row">
                <div class="col-lg">
                  <img src="{{ asset('public/images/2.png') }}" alt="{{ asset('public/images/two.png') }}" class="img-fluid" width="90">

                </div>
                <div class="col-lg text-center">
                  <h5 class="text-dark">Total <br> Events</h5> <br>
                  <h3 class="text-center text-dark"> {{ $events }}</h3>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg">
          <div class="card card-pricing popular text-center z-depth-3">
            <div class="card-body">
              <div class="row">
                <div class="col-lg">
                  <img src="{{ asset('public/images/3.png') }}" alt="{{ asset('public/images/two.png') }}" class="img-fluid" width="90">

                </div>
                <div class="col-lg text-center">
                  <h5 class="text-dark">Total Active <br> Members</h5> <br>
                  <h3 class="text-center text-dark">0</h3>
                </div>
              </div>
            </div>
          </div>
        </div>


      </div>

    </div>

@endsection
