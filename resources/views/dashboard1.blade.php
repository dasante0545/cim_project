@extends('layouts.admin1')
@section('page-css')

@endsection
@section('page-content')

      <section class="content">

        <div class="content-inner content-docs">
          <div class="row">
            <div class="col-lg-3 col-md-3 mb-5">
              <div class="card bg-tertiary">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg">
{{--                      <img src="{{ asset('images/1.png') }}" alt="{{ asset('images/one.png') }}" class="img-fluid" width="90">--}}

                    </div>
                    <div class="col-lg text-center">
                      <h5 class="text-white">Total <br> Users</h5> <br>
                      <h3 class="text-center text-white">1</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-3 mb-5">
              <div class="card" style="background-color:#e1cd56;">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg">
{{--                      <img src="{{ asset('images/2.png') }}" alt="{{ asset('images/two.png') }}" class="img-fluid" width="90">--}}

                    </div>
                    <div class="col-lg text-center">
                      <h5 class="text-white">Total <br> Pending</h5> <br>
                      <h3 class="text-center text-white">2</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-3 mb-5">
              <div class="card bg-primary">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg">
{{--                      <img src="{{ asset('images/3.png') }}" alt="{{ asset('images/two.png') }}" class="img-fluid" width="90">--}}

                    </div>
                    <div class="col-lg text-center">
                      <h5 class="text-white">Total <br> Started</h5> <br>
                      <h3 class="text-center text-white">4</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-3 mb-5">
              <div class="card bg-success">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg">
{{--                      <img src="{{ asset('images/4.png') }}" alt="{{ asset('images/one.png') }}" class="img-fluid" width="90">--}}
                    </div>
                    <div class="col-lg text-center ">
                      <h5 class="text-white">Total<br> Completed</h5> <br>
                      <h3 class="text-center text-white">4</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </div>
@endsection

@section('page-js')
{{--  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>--}}

{{--  @if($chart)--}}
{{--  {!! $chart->script() !!}--}}
{{--  @endif--}}
@endsection
