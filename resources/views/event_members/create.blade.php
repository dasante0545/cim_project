@extends('layouts.admin')

@section('title', 'Members Event')
@section('admin_css')

    <link rel="stylesheet" href="{!! asset('css/dataTables.bootstrap.min.css') !!}" type="text/css">
    <link rel="stylesheet" href="{!! asset('css/dataTables.bootstrap4.css') !!}" type="text/css">

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css"/>
    {{--    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />--}}
    {{--    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.7/css/fixedHeader.dataTables.min.css" />--}}
    {{--    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" />--}}

    {{--    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" />--}}
    {{--    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">--}}
    {{--    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">--}}
@endsection
@section('content')


    <div class="">

        <form method="POST" action="{{route('add.members.to.event', $event)}}">
            @csrf
            <div class="card">
                <div class="card-header">
                    Add Members To Event List

                    <div class="float-right">
                        <a href="{{ route('event_categories.index') }}" class="btn btn-tertiary btn-sm"><i class="fa fa-arrow-circle-left"></i> Go to Event Category</a>
                        <button type="submit" class="btn btn-sm btn-primary">Save & Apply</button>
    {{--                    <a href="{{ route('members.create') }}" class="btn btn-tertiary text-white btn-sm"><i class="fa fa-plus-circle"></i> Add Member</a>--}}
                    </div>
                </div>

                <div class="card-body">
                        <div class="table-responsive">
                        <div class="row">
                          <div class="col-lg-4 mx-auto">
                            @include('includes.alert')
                          </div>
                        </div>

                           <div class="row mb-5 cols-xs-space cols-sm-space cols-md-space">
                               <div class="col-md col-6">
                                   <div class="px-4 py-2 border text-center h6 border-tertiary rounded">
                                       Total Members <br> {{ $total = \App\Models\Member::all()->count() }}
                                   </div>
                               </div>
                                <div class="col-md col-6">
                                  <div class="px-4 py-2 border border-success text-center h6 rounded">
                                    No. Attended <br> @if($event->status != 'pending') {{ $members->count() }} @else 0 @endif
                                  </div>
                                </div>
                                <div class="col-md col-6">
                                  <div class="px-4 py-2 border text-center h6 border-danger rounded">
                                    No. Absented <br> @if($event->status != 'pending') {{ $total - $members->count() }} @else 0 @endif
                                  </div>
                                </div>
                           </div>
                        <!--begin: Datatable-->
                        {!! $dataTable->table(['class'=> 'table table-striped table-bordered',]) !!}
                        <!--end: Datatable-->
                        </div>

                        <input type="hidden" value="{{ $members }}" id="members">
                        <input type="hidden" value="{{ $event }}" id="event">
                </div>
            </div>
        </form>
    </div>



@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="{!! asset('vendor/datatables/buttons.server-side.js') !!}"></script>
    <script src="{!! asset('js/dataTables.buttons.min.js') !!}"></script>
    <script src="{!! asset('js/buttons.server-side.js') !!}"></script>
    {!! $dataTable->scripts() !!}


    <script>
        const table = $('#addmemberstoeventdatatable-table');
        table.on('preXhr.dt', function (e, settings,data) {
            data.members = $('#members').val();
            data.event = $('#event').val();

            // console.log(data.members, data.event);

        });

        // $('#generate').on('click', function () {
        //     table.DataTable().ajax.reload();
        //     return false;
        // })

    </script>

@endpush
