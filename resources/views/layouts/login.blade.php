<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ChristCare | Login</title>
    <link rel="stylesheet" href="{{ asset('public/css/demo.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/theme.css') }}">
    <meta name="google-site-verification" content="K8N68i2PJDrCUqrFDWycHgQhEHW_8XS7kHCJ5XzDzZY" />
    @yield('page-css')
    <link rel="shortcut icon" href="/images/mtnlogo.svg">

  </head>
  <body data-spy="scroll" data-target="#listexample" data-offset="0">

@yield('page-content')


    <script src="{{ asset('public/vendor/jquery/jquery.min.js') }}" ></script>
    <script src="{{ asset('public/vendor/popper/popper.min.js') }}" ></script>
    <script src="{{ asset('public/js/bootstrap/bootstrap.min.js') }}" ></script>
    <script src="{{ asset('public/js/theme.js') }}" ></script>
    <script src="{{ asset('public/vendor/fontawesome/js/fontawesome-all.min.js') }}" defer></script>

    @yield('page-js')
  </body>
</html>
