<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>CIM||Admin</title>
      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,800|Roboto:400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="{!! asset('public/css/demo.css') !!}">
    <link rel="stylesheet" href="{!! asset('public/css/theme.css') !!}">
      <link rel="stylesheet" href="{!! asset('public/vendor/jquery-scrollbar/css/jquery-scrollbar.css') !!}">
      <link type="text/css" href="{!! asset('public/vendor/highlight/css/highlight.min.css') !!}" rel="stylesheet">
    @yield('page-css')
  </head>
  <body data-spy="scroll" data-target="#listexample" data-offset="0">
    <main class="main">
      <aside class="sidebar" id="nav_docs">
            <div class="sidebar-brand text-center " style="background-color:white">
{{--              <img src="{{ asset('public/images/mtnlogo.svg') }}" class=" text-center" width="60" height="60">--}}
              <h1 class="font-weight-400"><a href="../"><span class="font-weight-700">CIM</span></a></h1>
            </div>
            <div class="scrollbar-inner mt-2">
              <ul class="navigation pr-3">
                  <li class="navigation-title">Getting started</li>
                  <li class="navigation-item">
                      <a href="introduction.html" class="navigation-link">Introduction</a>
                  </li>
                  <li class="navigation-item">
                      <a href="file-structure.html" class="navigation-link">File structure</a>
                  </li>
                  <li class="navigation-item">
                      <a href="plugins.html" class="navigation-link">Plugins</a>
                  </li>
                  <li class="navigation-item">
                      <a href="customization.html" class="navigation-link">Customization</a>
                  </li>
                  <li class="navigation-item">
                      <a href="update.html" class="navigation-link">Update</a>
                  </li>
                  <li class="navigation-title">Design elements</li>
                  <li class="navigation-item">
                      <a href="colors.html" class="navigation-link">Colors</a>
                  </li>
                  <li class="navigation-item">
                      <a href="typography.html" class="navigation-link">Typography</a>
                  </li>
                  <li class="navigation-item">
                      <a href="icons.html" class="navigation-link">Icons</a>
                  </li>
                  <li class="navigation-title">Components</li>
                  <li class="navigation-item">
                      <a href="alerts.html" class="navigation-link">Alerts</a>
                  </li>
                  <li class="navigation-item">
                      <a href="avatars.html" class="navigation-link">Avatars</a>
                  </li>
                  <li class="navigation-item">
                      <a href="badges.html" class="navigation-link">Badges</a>
                  </li>
                  <li class="navigation-item">
                      <a href="buttons.html" class="navigation-link">Buttons</a>
                  </li>
                  <li class="navigation-item">
                      <a href="cards.html" class="navigation-link">Cards</a>
                  </li>
                  <li class="navigation-item">
                      <a href="dropdowns.html" class="navigation-link">Dropdowns</a>
                  </li>
                  <li class="navigation-item">
                      <a href="forms.html" class="navigation-link">Forms</a>
                  </li>
                  <li class="navigation-item">
                      <a href="modal.html" class="navigation-link">Modal</a>
                  </li>
                  <li class="navigation-item">
                      <a href="navbar.html" class="navigation-link">Navbar</a>
                  </li>
                  <li class="navigation-item">
                      <a href="navs.html" class="navigation-link">Navs</a>
                  </li>
                  <li class="navigation-item">
                      <a href="pagination.html" class="navigation-link">Pagination</a>
                  </li>
                  <li class="navigation-item">
                      <a href="progress.html" class="navigation-link">Progress</a>
                  </li>
                  <li class="navigation-item">
                      <a href="tables.html" class="navigation-link">Tables</a>
                  </li>
                  <li class="navigation-item">
                      <a href="tooltips.html" class="navigation-link">Tooltips</a>
                  </li>
              </ul>
            </div>
          </aside>
        <div class="header">
            <div class="navigation-trigger d-xl-none" data-action="aside-open" data-target=".sidebar">
              <div class="navigation-trigger-inner">
                <i class="navigation-trigger-line"></i>
                <i class="navigation-trigger-line"></i>
                <i class="navigation-trigger-line"></i>
              </div>
            </div>
            <div class="header-brand d-xl-none">
              <h1><a href="#">CIM</a></h1>
            </div>
            <div class="header-nav ml-auto">
              <ul class="nav align-items-center">

                <li class="nav-item ml-4">
                  <a href="#" class="btn btn-sm text-white" style="background-color:#ffcb05;">Logout</a>
                </li>
              </ul>
            </div>
          </div>

        @yield('page-content')
          <div class="card-footer mt-5" style=" position: fixed; left: 0; bottom: 0; width: 100%; text-align: center;">
            <p class="p-0 m-0 xs-font-size13 text-center" >© Powered by <a href="https://npontu.com" class="text-dark"> CIM Technologies
{{--          <img src="{!! asset('public/images/npontu.jpeg') !!}" class="img-fluid" alt="Npontu Logo" width="50">--}}
                </a></p>
          </div>
</section>
    </main>


    <script src="{!! asset('public/vendor/jquery/jquery.min.js') !!}" ></script>
    <script src="{!! asset('public/vendor/popper/popper.min.js') !!}" ></script>
    <script src="{!! asset('public/js/bootstrap/bootstrap.min.js') !!}" ></script>
    <script src="{!! asset('public/js/theme.js') !!}" ></script>
    <script src="{!! asset('public/js/demo.js') !!}" ></script>
    <script src="{!! asset('public/vendor/fontawesome/js/fontawesome-all.min.js') !!}" ></script>

    <script src="{!! asset('public/vendor/jquery-scrollbar/js/jquery-scrollbar.min.js') !!}" ></script>
    <script src="{!! asset('public/vendor/jquery-scrollLock/jquery-scrollLock.min.js') !!}" ></script>
    <script src="{!! asset('public/vendor/sticky-kit/sticky-kit.min.js') !!}" ></script>
    <script src="{!! asset('public/vendor/highlight/js/highlight.min.js') !!}" ></script>
    <script src="{!! asset('public/vendor/clipboard-js/clipboard.min.js') !!}" ></script>
    <script src="{!! asset('public/vendor/holderjs/holder.min.js') !!}" ></script>

    @yield('page-js')
  </body>
</html>
