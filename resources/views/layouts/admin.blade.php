<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Probably the most complete UI kit out there. Multiple functionalities and controls added,  extended color palette and beautiful typography, designed as its own extended version of Bootstrap at  the highest level of quality.">
    <meta name="author" content="Webpixels">
    <title>ChristCare | @yield('title', 'CIM')</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,800|Roboto:400,500,700" rel="stylesheet">
    <!-- Plugins -->
    <link rel="stylesheet" href="{!! asset('public/vendor/animate/animate.min.css') !!}" type="text/css">
    <link type="text/css" href="{!! asset('public/css/theme.css') !!}" rel="stylesheet">
    <link rel="stylesheet" href="{!! asset('public/vendor/jquery-scrollbar/css/jquery-scrollbar.css') !!}">
    <link type="text/css" href="{!! asset('public/vendor/highlight/css/highlight.min.css') !!}" rel="stylesheet">
    <link type="text/css" href="{!! asset('public/css/demo.css') !!}" rel="stylesheet">
    @yield('admin_css')
</head>
<body>
<main class="main">
    <aside class="sidebar" id="nav_docs">
      <div class="sidebar-brand text-center " style="background-color:white">
              <img src="{{ asset('public/images/care.jpeg') }}" class=" text-center" width="60" height="60">
              <h1 class="font-weight-400"><a href="{{ route('home') }}"><span class="font-weight-700">ChristCare</span> Int. Min</a></h1>
            </div>
<br> <br>
        <div class="scrollbar-inner">
            <ul class="navigation pr-3">
                {{--   MEMBERSHIP   --}}

                <li class="navigation-title">MEMBERSHIP</li>
                <li class="navigation-item">
                    <a href="{{ route('home') }}" class="navigation-link">Dashboard</a>
                    <a href="{{ route('members.index') }}" class="navigation-link">Members</a>
                </li>

                {{--   EVENT MANAGER   --}}
                <li class="navigation-title">EVENT MANAGER</li>
                <li class="navigation-item">
                    <a href="{{ route('event_categories.index') }}" class="navigation-link">Event Category</a>
                </li>

                {{--   SETUP MANAGER   --}}
                <li class="navigation-title">SETUP MANAGER</li>
                <li class="navigation-item">
                    <a href="{{ route('groups.index') }}" class="navigation-link">Groups</a>
                </li>
                <li class="navigation-item">
                    <a href="{{ route('departments.index') }}" class="navigation-link">Departments</a>
                </li>


            </ul>
        </div>
    </aside>

    <section class="content">

        {{--   SIDEBAR IN MOBILE VIEW   --}}
        <div class="header sticky-top">
            <div class="navigation-trigger " data-action="aside-open" data-target=".sidebar">
                <div class="navigation-trigger-inner">
                    <i class="navigation-trigger-line"></i>
                    <i class="navigation-trigger-line"></i>
                    <i class="navigation-trigger-line"></i>
                </div>
            </div>
            <div class="header-brand d-xl-none">
                <h1><a href="index.html">ChristCare Int. Min</a></h1>
            </div>

            {{--   NAVBAR   --}}
            <div class="header-nav ml-auto">
                <ul class="nav align-items-center">
                    {{-- <li class="nav-item d-none d-md-inline-block">
                        <a class="nav-link pl-0 active" href="../">Go to UI Kit</a>
                    </li> --}}
                    {{-- <li class="nav-item d-none d-md-inline-block">
                        <a class="nav-link" href="../docs/changelog.html">Changelog</a>
                    </li> --}}
                    {{-- <li class="nav-item ml-4">
                        <a href="https://github.com/webpixels/boomerang-ui-kit/archive/master.zip" class="btn btn-sm btn-primary">Download Free</a>
                    </li> --}}

                    <li class="nav-item ml-4">
                        <a href="{{ route('logout') }}" onclick="return confirm('Do you want to logout ?')" class="btn btn-sm btn-primary">logout</a>
                    </li>

                </ul>
            </div>
        </div>


        {{--   MAIN CONTENT   --}}
        <div class="content-inner content-docs mb-5">

            @yield('content')

        </div>

        <div class="card-footer mt-5" style=" position: fixed; left: 0; bottom: 0; width: 100%; text-align: center;">
{{--            <p class="p-0 m-0 xs-font-size13 text-center" >© Powered by <a href="https://npontu.com" class="text-dark"> CIM Technologies--}}
{{--                </a></p>--}}
            <div class="">&copy; 2021 <a href="https://webpixels.io/" target="_blank">ChristCare</a>. All rights reserved.</div>

        </div>

        {{--   FOOTER   --}}
{{--        <footer class="footer bg-white fixed-bottom mt-sm-5">--}}
{{--            <div class="container">--}}
{{--                <div class="row align-items-center py-3 py-sm-1 border-top">--}}
{{--                    <div class="col-md-6 text-center text-lg-left mb-2 mb-lg-0">--}}
{{--                        &copy; 2021 <a href="https://webpixels.io/" target="_blank">ChristCare</a>. All rights reserved.--}}
{{--                    </div>--}}
{{--                    <div class="col-lg-6 text-center text-lg-right">--}}
{{--                        <ul class="nav justify-content-center justify-content-lg-end">--}}
{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link active" href="https://instagram.com/webpixelsofficial" target="_blank"><i class="fab fa-instagram"></i></a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link" href="https://facebook.com/webpixels" target="_blank"><i class="fab fa-facebook"></i></a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link" href="https://github.com/webpixels" target="_blank"><i class="fab fa-github"></i></a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link" href="https://dribbble.com/webpixels" target="_blank"><i class="fab fa-dribbble"></i></a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </footer>--}}
    </section>
</main>
<!-- Core Script-->
<script src="{!! asset('public/vendor/jquery/jquery.min.js') !!}"></script>
<script src="{!! asset('public/vendor/popper/popper.min.js') !!}"></script>
<script src="{!! asset('public/js/bootstrap/bootstrap.min.js') !!}"></script>
<!-- FontAwesome 5 -->
<script src="{!! asset('public/vendor/fontawesome/js/fontawesome-all.min.js') !!}" defer></script>
<script src="{!! asset('public/vendor/jquery-scrollbar/js/jquery-scrollbar.min.js') !!}"></script>
<script src="{!! asset('public/vendor/jquery-scrollLock/jquery-scrollLock.min.js') !!}"></script>
<script src="{!! asset('public/vendor/sticky-kit/sticky-kit.min.js') !!}"></script>
<script src="{!! asset('public/vendor/highlight/js/highlight.min.js') !!}"></script>
<script src="{!! asset('public/vendor/clipboard-js/clipboard.min.js') !!}"></script>
<script src="{!! asset('public/vendor/holderjs/holder.min.js') !!}"></script>
<script src="{!! asset('public/vendor/bootstrap-select/js/bootstrap-select.min.js') !!}"></script>
<script src="{!! asset('public/js/demo.js') !!}"></script>
<script src="{!! asset('public/js/theme.js') !!}"></script>
@stack('scripts')
@yield('admin_js')
</body>
</html>
