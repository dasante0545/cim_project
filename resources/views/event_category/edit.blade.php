@extends('layouts.admin')

@section('title', 'Event Category')
@section('content')

<div class="row">
  <div class="col-lg-5 mx-auto">
    <div class="card">
      <div class="card-header">
        Edit Event Category
      </div>
      <div class="card-body">
        <form method="POST" action="{{route('event_categories.update', $eventCategory->id)}}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label>Category Name</label>
                <input type="text" name="name" value="{{ old('name') ?? $eventCategory->name }}" class="form-control @error('name') is-invalid @enderror">
                @error('name')
                <span class="text-danger" role="alert">
                       {{ $message }}
                   </span>
                @enderror
            </div>
            <div class="text-center">
              <a href="{{ route('event_categories.index') }}" class="btn btn-sm btn-danger  text-white mr-2"> <i class="fa fa-sm fa-arrow-left"></i> Cancel</a>

              <button type="submit" class="btn btn-sm btn-primary"> Update </button>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>


@endsection
