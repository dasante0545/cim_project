@extends('layouts.admin')

@section('admin_css')

    <link rel="stylesheet" href="{!! asset('css/dataTables.bootstrap.min.css') !!}" type="text/css">
    <link rel="stylesheet" href="{!! asset('css/dataTables.bootstrap4.css') !!}" type="text/css">

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css"/>
    {{--    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />--}}
    {{--    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.7/css/fixedHeader.dataTables.min.css" />--}}
    {{--    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" />--}}

    {{--    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" />--}}
    {{--    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">--}}
    {{--    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">--}}
@endsection
@section('content')


    <div class="">

        <div class="card">
            <div class="card-header">
                 <span>{{ $member->name }} 's Event list</span>

                <div class="float-right">
                    <a href="{{ route('members.index') }}" class="btn btn-danger text-white btn-sm"><i class="fa fa-backward"></i> Go Back</a>
                    <button type="button" class="btn btn-sm btn-light" data-toggle="modal" data-target="#modal_1">Summary </button>
                </div>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <div class="row">
                        <div class="col-lg-4 mx-auto">
                            @include('includes.alert')
                        </div>
                    </div>
{{--            <div class="row mb-5 cols-xs-space cols-sm-space cols-md-space">--}}
{{--                <div class="col-md col-6">--}}
{{--                  <div class="px-4 py-4 border border-tertiary text-center h6 rounded">--}}
{{--                    No. Attended <br> 17--}}
{{--                  </div>--}}
{{--                </div>--}}
{{--                <div class="col-md col-6">--}}
{{--                  <div class="px-4 py-4 border text-center h6 border-info rounded">--}}
{{--                    No. Absented <br> 7--}}
{{--                  </div>--}}
{{--                </div>--}}
{{--                <div class="col-md col-6">--}}
{{--                  <div class="px-4 py-4 border text-center h6 border-warning rounded">--}}
{{--                    % of Attended <br> 17%--}}
{{--                  </div>--}}
{{--                </div>--}}
{{--                <div class="col-md col-6">--}}
{{--                  <div class="px-4 py-4 border text-center h6 border secondary rounded">--}}
{{--                  % of Attended <br> 7%--}}
{{--                  </div>--}}
{{--                </div>--}}
{{--            </div>--}}
                    <!--begin: Datatable-->
                {!! $dataTable->table(['class'=> 'table table-striped table-bordered',]) !!}
                <!--end: Datatable-->

                    <input type="hidden" value="{{ $events }}" id="events">
                </div>
            </div>
        </div>

    </div>


{{-- Modal --}}
<div class="modal fade" id="modal_1" tabindex="-1" role="dialog" aria-labelledby="modal_1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_title_6">{{ $member->name }}'s Summary</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row col-md-12">
{{--                    <div class="col-lg-12 text-center py-4">--}}
                        <div>
                            <div class="row">
                                <div>No. Events <i class="fa fa-forward"></i> <span class="btn btn-sm btn-tertiary mr-2"> {{ $counts['events_finished'] }}</span> </div>

                                <div>No. Attended <i class="fa fa-forward"></i> <span class="btn btn-sm btn-success mr-2"> {{ $counts['no_attended'] }}</span></div>

                                <div class="">No. Absented <i class="fa fa-forward"></i> <span class="btn btn-sm btn-danger"> {{ $counts['no_absented'] }}</span></div>
                            </div>



                        </div>

{{--                        <h4 class="heading h3">{{ $member->name }}'s Summary</h4>--}}
{{--                        <p class="lead text-muted">--}}
{{--                            <div class="row">--}}
{{--                              <div class="col-lg">--}}
{{--                                <h6>Total No. of Events Attended:</h6>--}}
{{--                                <h6>Total No. of Events Absented:</h6>--}}
{{--                                <h6>Total % of Events Attended:</h6>--}}
{{--                                <h6>Total % of Events Attended:</h6>--}}
{{--                              </div>--}}
{{--                              <div class="col-lg">--}}
{{--                                <h6 class="text-success h6">39</h6>--}}
{{--                                <h6 class="text-primary h6">3</h6>--}}
{{--                                <h6 class="text-success h6">87%</h6>--}}
{{--                                <h6 class="text-tertiary h6">9%</h6>--}}
{{--                              </div>--}}
{{--                            </div>--}}
{{--                        </p>--}}

{{--                    </div>--}}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
{{-- Modal --}}
@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="{!! asset('vendor/datatables/buttons.server-side.js') !!}"></script>
    <script src="{!! asset('js/dataTables.buttons.min.js') !!}"></script>
    <script src="{!! asset('js/buttons.server-side.js') !!}"></script>
    {!! $dataTable->scripts() !!}

    <script>
        const table = $('#eventsattendeddatatable-table');
        table.on('preXhr.dt', function (e, settings,data) {
            data.events = $('#events').val();

            // console.log(data.events);

        });


    </script>

@endpush
