@extends('layouts.admin')


@section('content')

    <div class="card">
        <div class=" ">
            {{-- <div class="row border-bottom">
                <div class="col-lg-6">

                </div>
            </div> --}}
<div class="card-header">
  <h2 class="heading h2 font-weight-bold">Register Member</h2>
</div>
        <div class="card-body">
          <form method="POST" action="{{route('members.store')}}">
              @csrf
              <div class="row ">
                <div class="col-lg">
                  <div class="form-group ">
                      <label>First & Middle Name</label>
                      <input type="text" name="first_name" value="{{ old('first_name') }}" class="form-control @error('first_name') is-invalid @enderror" placeholder="">
                      @error('first_name')
                         <span class="text-danger" role="alert">
                             {{ $message }}
                         </span>
                      @enderror
                  </div>

                  <div class="form-group ">
                      <label>Last Name</label>
                      <input type="text" name="last_name" value="{{ old('last_name') }}" class="form-control @error('last_name') is-invalid @enderror" placeholder="">
                      @error('last_name')
                      <span class="text-danger" role="alert">
                             {{ $message }}
                         </span>
                      @enderror
                  </div>

                  <div class="form-group ">
                      <label>Mobile Number</label>
                      <input type="tel" name="mobile_number" value="{{ old('mobile_number') }}" class="form-control @error('mobile_number') is-invalid @enderror" placeholder="eg. 233545000000">
                      @error('mobile_number')
                      <span class="text-danger" role="alert">
                             {{ $message }}
                         </span>
                      @enderror
                  </div>

                  <div class="form-group ">
                      <label>Emergency Number</label>
                      <input type="tel" name="emergency_contact" value="{{ old('emergency_contact') }}" class="form-control @error('emergency_contact') is-invalid @enderror" placeholder="eg. 233545000000">
                      @error('emergency_contact')
                      <span class="text-danger" role="alert">
                             {{ $message }}
                         </span>
                      @enderror
                  </div>
                  <div class="form-group ">
                      <label>Email</label>
                      <input type="email" name="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" placeholder="eg. media@cim.com">
                      @error('email')
                      <span class="text-danger" role="alert">
                             {{ $message }}
                         </span>
                      @enderror
                  </div>
                </div>
                <div class="col-lg">
                  <div class="form-group ">
                      <label>Residential Address</label>
                      <input type="text" name="residential_address" value="{{ old('residential_address') }}" class="form-control @error('residential_address') is-invalid @enderror" placeholder="eg. Ashaiman-Newtown">
                      @error('residential_address')
                      <span class="text-danger" role="alert">
                             {{ $message }}
                         </span>
                      @enderror
                  </div>

                  <div class="form-group ">
                      <label>Date of Birth</label>
                      <input type="date" name="date_of_birth" value="{{ old('date_of_birth') }}" class="form-control @error('date_of_birth') is-invalid @enderror" placeholder="">
                      @error('date_of_birth')
                      <span class="text-danger" role="alert">
                             {{ $message }}
                         </span>
                      @enderror
                  </div>

                  <div class="form-group ">
                      <label>Occupation</label>
                      <input type="text" name="occupation" value="{{ old('occupation') }}" class="form-control @error('occupation') is-invalid @enderror" placeholder="eg. artisan">
                      @error('occupation')
                      <span class="text-danger" role="alert">
                             {{ $message }}
                         </span>
                      @enderror
                  </div>
                  <div class="form-group ">
                      <label>Marital Status</label>
                      <select name="marital_status" class="selectpicker" title="select marital status" data-live-search="true" data-live-search-placeholder="Search ...">
                          <option value="single" {{ old('marital_status') == 'single' ? 'selected' : '' }}>Single</option>
                          <option value="married" {{ old('marital_status') == 'married' ? 'selected' : '' }}>Married</option>
                          <option value="divorced" {{ old('marital_status') == 'divorced' ? 'selected' : '' }}>Divorced</option>
                          <option value="widowed" {{ old('marital_status') == 'widowed' ? 'selected' : '' }}>Widowed</option>
                      </select>
                      @error('marital_status')
                      <span class="text-danger" role="alert">
                             {{ $message }}
                         </span>
                      @enderror
                  </div>

                  <div class="form-group ">
                      <label>Number of Children if any</label>
                      <input type="number" name="no_of_children" value="{{ old('no_of_children') }}" class="form-control @error('no_of_children') is-invalid @enderror" placeholder="eg. 3" min="0" max="30">
                      @error('no_of_children')
                      <span class="text-danger" role="alert">
                             {{ $message }}
                         </span>
                      @enderror
                  </div>
                </div>

              <div class="col-lg">
                  <div class="form-group">
                      <label>Membership Id</label>
                      <div class="input-group">
                          <div class="input-group-prepend">
                              <select class="selectpicker" name="id_prefix" disabled>
                                  <option>CIMBT</option>
                              </select>
                          </div>
                          <input type="text" class="form-control" name="membership_id" value="{{ old('membership_id') ? (int) filter_var(old('membership_id'), FILTER_SANITIZE_NUMBER_INT) : '' }}" minlength="1" maxlength="5" required>
                          @error('membership_id')
                          <span class="text-danger" role="alert">
                            {{ $message }}
                          </span>
                          @enderror
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-4">
                          <label class="d-block">Only Year</label>
                          <label class="toggle-switch">
                              <input type="checkbox" id="toggle-check">
                              <span class="toggle-switch-slider rounded-circle"></span>
                          </label>

                      </div>

                      <div class="form-group col-md-8" id="date_joined">
                          <label>Date You Joined Church</label>
                          <input type="date" name="date_joined" value="{{ old('date_joined') }}" class="form-control @error('date_joined') is-invalid @enderror" placeholder="">
                          @error('date_joined')
                          <span class="text-danger" role="alert">
                            {{ $message }}
                          </span>
                          @enderror
                      </div>

                      <div class="form-group col-md-8" id="year_joined">
                          <label>Year Joined Church</label>
                          <select name="year_joined" id="yearpicker" class="selectpicker form-control @error('year_joined') is-invalid @enderror" data-live-search="true" data-live-search-placeholder="Search ...">
                              <option></option>
                          </select>
                          @error('year_joined')
                          <span class="text-danger" role="alert">
                            {{ $message }}
                          </span>
                          @enderror
                      </div>
                  </div>


{{--                      <div class="form-group ">--}}
{{--                          <label>Month Year Joined Church</label>--}}
{{--                          <input type="month" name="month_year_joined" value="{{ old('month_year_joined_church') }}" class="form-control @error('month_year_joined_church') is-invalid @enderror" placeholder="">--}}
{{--                          @error('month_year_joined_church')--}}
{{--                          <span class="text-danger" role="alert">--}}
{{--                             {{ $message }}--}}
{{--                         </span>--}}
{{--                          @enderror--}}
{{--                      </div>--}}

                  <div class="form-group ">
                      <label>Group</label>
                      <select name="group_id" class="selectpicker" title="select group" data-live-search="true" data-live-search-placeholder="Search ...">
                          @foreach($groups as $group)
                            <option value="{{ $group->id }}" {{ old('group_id') == $group->id ? 'selected' : '' }}>{{ $group->group }}</option>
                          @endforeach
                      </select>
                      @error('group_id')
                      <span class="text-danger" role="alert">
                     {{ $message }}
                 </span>
                      @enderror
                  </div>

                  <div class="form-group ">
                      <label>Department</label>
                      <select name="departments[]" class="selectpicker" title="select departments" data-live-search="true" data-live-search-placeholder="Search ..." multiple>
                          @foreach($departments as $department)
                            <option value="{{ $department->id }}" {{ old('departments') ? (in_array($department->id,old('departments')) ? 'selected' : '') : '' }}>{{ $department->department }}</option>
                          @endforeach
                      </select>
                      @error('departments')
                      <span class="text-danger" role="alert">
                     {{ $message }}
                 </span>
                      @enderror
                  </div>
              </div>
              </div>


            <div class="text-center m-4">
              <a href="{{ route('members.index') }}" class="btn btn-sm btn-danger col-lg-3">Cancel</a>
              <button type="submit" class="btn btn-sm btn-primary col-lg-3">Save & Apply</button>
            </div>

          </form>
        </div>

        </div>

    </div>
@endsection

@section('admin_js')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js">
    </script>
    <script type="text/javascript">
        let startYear = 1800;
        let endYear = new Date().getFullYear();
        for (i = endYear; i > startYear; i--)
        {
            $('#yearpicker').append($('<option />').val(i).html(i));
        }
    </script>

    <script>
        $(document).ready( function (){
            $("#year_joined").hide();

            //show it when the checkbox is clicked
            $("#toggle-check").on('click', function (){
                if ($(this).prop('checked')){
                    $("#year_joined").show();
                    $("#date_joined").hide(function (){
                        $(this).val(null);
                    });


                }else{
                    $("#year_joined").hide(function (){
                        $(this).val(null);
                    });
                    $("#date_joined").show();
                }
            })
        })
    </script>

@endsection
