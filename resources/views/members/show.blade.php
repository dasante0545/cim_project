@extends('layouts.admin')


@section('content')

  <div class="content-inner content-docs">
    <h2 class="heading h2 font-weight-bold">Member Details</h2> <hr> <br>
      <div class="row">
        <div class="col-lg">
          <img src="{!! asset('images/1.png') !!}" class="img-fluid" alt="Profile Image">
        </div>
        <div class="col-lg">
          <div class="card m-1">
              <div class="card-body">
                <div class="d-flex align-items-start">
                  <div class="icon-text">
                    <h3 class="heading h5">First & Middle Name</h3>
                    <p class="mb-0">{{ $member->first_name }}</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="card m-1 bg-secondary">
                <div class="card-body">
                  <div class="d-flex align-items-start">
                    <div class="icon-text">
                      <h3 class="heading h5">Last Name</h3>
                      <p class="mb-0">{{ $member->last_name }}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card m-1">
                  <div class="card-body">
                    <div class="d-flex align-items-start">
                      <div class="icon-text">
                        <h3 class="heading h5">Mobile Number</h3>
                        <p class="mb-0">{{ $member->mobile_number }}</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card m-1 bg-secondary">
                    <div class="card-body">
                      <div class="d-flex align-items-start">
                        <div class="icon-text">
                          <h3 class="heading h5">Emergency Number</h3>
                          <p class="mb-0">{{ $member->emergency_contact }}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card m-1">
                      <div class="card-body">
                        <div class="d-flex align-items-start">
                          <div class="icon-text">
                            <h3 class="heading h5">Email</h3>
                            <p class="mb-0">{{ $member->email }}</p>
                          </div>
                        </div>
                      </div>
                    </div>
        </div>
        <div class="col-lg">
          <div class="card m-1 bg-secondary">
              <div class="card-body">
                <div class="d-flex align-items-start">
                  <div class="icon-text">
                    <h3 class="heading h5">Residential Address</h3>
                    <p class="mb-0">{{ $member->residential_address }}</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="card m-1">
                <div class="card-body">
                  <div class="d-flex align-items-start">
                    <div class="icon-text">
                      <h3 class="heading h5">Date of Birth</h3>
                      <p class="mb-0">{{ \Illuminate\Support\Carbon::parse($member->date_of_birth)->format('D d F, Y') }}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card m-1 bg-secondary">
                  <div class="card-body">
                    <div class="d-flex align-items-start">
                      <div class="icon-text">
                        <h3 class="heading h5">Occupation</h3>
                        <p class="mb-0">{{ $member->occupation }}</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card m-1">
                    <div class="card-body">
                      <div class="d-flex align-items-start">
                        <div class="icon-text">
                          <h3 class="heading h5">Marital Status</h3>
                          <p class="mb-0">{{ ucfirst($member->marital_status) }}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card m-1 bg-secondary">
                      <div class="card-body">
                        <div class="d-flex align-items-start">
                          <div class="icon-text">
                            <h3 class="heading h5">Number of Children if any</h3>
                            <p class="mb-0">{{ $member->no_of_children }}</p>
                          </div>
                        </div>
                      </div>
                    </div>
        </div>

          <div class="col-lg">
              <div class="card m-1">
                  <div class="card-body">
                      <div class="d-flex align-items-start">
                          <div class="icon-text">
                              <h3 class="heading h5">Membership Id</h3>
                              <p class="mb-0">{{ $member->membership_id }}</p>
                          </div>
                      </div>
                  </div>
              </div>

              @if($member->year_joined)
              <div class="card m-1 bg-secondary">
                  <div class="card-body">
                      <div class="d-flex align-items-start">
                          <div class="icon-text">
                              <h3 class="heading h5">Year Joined Church</h3>
                              <p class="mb-0">{{ $member->year_joined }}</p>
                          </div>
                      </div>
                  </div>
              </div>

              @else
                  <div class="card m-1 bg-secondary">
                      <div class="card-body">
                          <div class="d-flex align-items-start">
                              <div class="icon-text">
                                  <h3 class="heading h5">Date Joined Church</h3>
                                  <p class="mb-0">{{ $member->date_joined }}</p>
                              </div>
                          </div>
                      </div>
                  </div>
              @endif

              <div class="card m-1">
                  <div class="card-body">
                      <div class="d-flex align-items-start">
                          <div class="icon-text">
                              <h3 class="heading h5">Group</h3>
                              <p class="mb-0">{{ $member->group->group }}</p>
                          </div>
                      </div>
                  </div>
              </div>

              <div class="card m-1 bg-secondary">
                  <div class="card-body">
                      <div class="d-flex align-items-start">
                          <div class="icon-text">
                              <h3 class="heading h5">Department(s)</h3>
                              @foreach($member->departments as $department)
                                <p class="mb-0">{{ $department->department }}</p>
                              @endforeach
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="text-center mt-5">
        <a href="{{ route('members.index') }}" class="btn col-lg-4  btn-sm btn-tertiary text-white mr-2"> <i class="fa fa-sm fa-arrow-left"></i> Go Back</a>
      </div>
    </div>

@endsection
