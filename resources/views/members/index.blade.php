@extends('layouts.admin')

@section('admin_css')

    <link rel="stylesheet" href="{!! asset('css/dataTables.bootstrap.min.css') !!}" type="text/css">
    <link rel="stylesheet" href="{!! asset('css/dataTables.bootstrap4.css') !!}" type="text/css">

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css"/>
{{--    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />--}}
{{--    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.7/css/fixedHeader.dataTables.min.css" />--}}
{{--    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" />--}}

{{--    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" />--}}
{{--    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">--}}
{{--    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">--}}
@endsection
@section('content')


    <div class="">

        <div class="card">
            <div class="card-header">
                Members List

                <div class="float-right">
                    <a href="{{ route('members.create') }}" class="btn btn-tertiary text-white btn-sm"><i class="fa fa-plus-circle"></i> Add Member</a>
                </div>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                <div class="row">
                  <div class="col-lg-6 mx-auto">
                    @include('includes.alert')
                  </div>
                </div>
                <!--begin: Datatable-->
                {!! $dataTable->table(['class'=> 'table table-striped table-bordered',]) !!}
                <!--end: Datatable-->
                </div>
            </div>
        </div>

    </div>



@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="{!! asset('vendor/datatables/buttons.server-side.js') !!}"></script>
    <script src="{!! asset('js/dataTables.buttons.min.js') !!}"></script>
    <script src="{!! asset('js/buttons.server-side.js') !!}"></script>
    {!! $dataTable->scripts() !!}

@endpush
