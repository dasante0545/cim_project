@extends('layouts.admin')

@section('title', 'Event')
@section('admin_css')

    <link rel="stylesheet" href="{!! asset('css/dataTables.bootstrap.min.css') !!}" type="text/css">
    <link rel="stylesheet" href="{!! asset('css/dataTables.bootstrap4.css') !!}" type="text/css">

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css"/>

@endsection
@section('content')


    <div class="">

        <div class="card">
            <div class="card-header">
                {{ $event_category->name }}  Event list

                <div class="float-right">
                    <a href="{{ route('event_categories.index') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-circle-left"></i> Go Back</a>
                    <a href="{{ route('events.create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add Event</a>
                </div>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                <div class="row">
                  <div class="mx-auto">
                    @include('includes.alert')
                  </div>
                </div>
                <!--begin: Datatable-->
                {!! $dataTable->table(['class'=> 'table table-striped table-bordered',]) !!}
                <!--end: Datatable-->
                </div>
            </div>
        </div>

    </div>


@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="{!! asset('vendor/datatables/buttons.server-side.js') !!}"></script>
    <script src="{!! asset('js/dataTables.buttons.min.js') !!}"></script>
    <script src="{!! asset('js/buttons.server-side.js') !!}"></script>
    {!! $dataTable->scripts() !!}

@endpush
