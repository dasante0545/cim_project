@extends('layouts.admin')


@section('content')

<div class="card">
  <div class="card-header">
    Create Event
  </div>
  <div class="card-body">
    <div class="col-lg-10 mx-auto">
      <form method="POST" action="{{route('events.store')}}">
          @csrf
          <div class="row">
            <div class="col-lg">
              <div class="form-group ">
                  <label>Event Name</label>
                  <input type="text" name="event_name" value="{{ old('event_name') }}" class="form-control @error('event_name') is-invalid @enderror" placeholder="">
                  @error('event_name')
                  <span class="text-danger" role="alert">
                         {{ $message }}
                     </span>
                  @enderror
              </div>


              <div class="form-group ">
                  <label>Event Date & Time</label>
                  <input type="datetime-local" name="event_date_time" value="{{ old('event_date_time') }}" class="form-control @error('event_date_time') is-invalid @enderror" placeholder="">
                  @error('event_date_time')
                  <span class="text-danger" role="alert">
                         {{ $message }}
                     </span>
                  @enderror
              </div>

              <div class="form-group ">
                  <label>Venue</label>
                  <input type="text" name="venue" value="{{ old('venue') }}" class="form-control @error('venue') is-invalid @enderror" placeholder="">
                  @error('venue')
                  <span class="text-danger" role="alert">
                         {{ $message }}
                     </span>
                  @enderror
              </div>
            </div>
            <div class="col-lg">
              <div class="form-group ">
                  <label>Comment</label>
                  <input type="text" name="comment" value="{{ old('comment') }}" class="form-control @error('comment') is-invalid @enderror" placeholder="">
                  @error('comment')
                  <span class="text-danger" role="alert">
                         {{ $message }}
                     </span>
                  @enderror
              </div>
              <div class="form-group ">
                  <label>Event Category</label>
                  <select name="category_id" class="selectpicker" title="select category" data-live-search="true" data-live-search-placeholder="Search ...">
                      @foreach($event_category as $category)
                          <option value="{{ $category->id }}" {{ old('category_id') == $category->id  ? 'selected' : '' }}>{{ $category->name }}</option>
                      @endforeach
                  </select>
                  @error('category_id')
                  <span class="text-danger" role="alert">
                     {{ $message }}
                 </span>
                  @enderror
              </div>

              <div>{{ request('category_id') }}</div>
            </div>
          </div>
          <div class="text-center">
            <a href="{{ url()->previous() }}" class="btn btn-sm col-lg-3 btn-danger">Cancel</a>
{{--                        <a href="{{ route('events.index', ['category_id' => request('category_id')]) }}" class="btn btn-sm btn-danger">Cancel</a>--}}
            <button type="submit" class="btn btn-sm col-lg-3 btn-primary">Save & Apply</button>
          </div>
        </form>
    </div>

  </div>
</div>


    {{-- <div class="container">
        <div class="pt-3 pb-4 mb-4 ">
            <div class="row border-bottom">
                <div class="col-lg-6">
                    <h2 class="heading h2 font-weight-bold">Create Event</h2>
                </div>
            </div>

            <form method="POST" action="{{route('events.store')}}">
                @csrf
                <div class="row mt-3 mb-4">
                    <div class="form-group col-md-3">
                        <label>Event Name</label>
                        <input type="text" name="event_name" value="{{ old('event_name') }}" class="form-control @error('event_name') is-invalid @enderror" placeholder="">
                        @error('event_name')
                        <span class="text-danger" role="alert">
                               {{ $message }}
                           </span>
                        @enderror
                    </div>


                    <div class="form-group col-md-3">
                        <label>Event Date & Time</label>
                        <input type="datetime-local" name="event_date_time" value="{{ old('event_date_time') }}" class="form-control @error('event_date_time') is-invalid @enderror" placeholder="">
                        @error('event_date_time')
                        <span class="text-danger" role="alert">
                               {{ $message }}
                           </span>
                        @enderror
                    </div>

                    <div class="form-group col-md-3">
                        <label>Venue</label>
                        <input type="text" name="venue" value="{{ old('venue') }}" class="form-control @error('venue') is-invalid @enderror" placeholder="">
                        @error('venue')
                        <span class="text-danger" role="alert">
                               {{ $message }}
                           </span>
                        @enderror
                    </div>

                    <div class="form-group col-md-3">
                        <label>Comment</label>
                        <input type="text" name="comment" value="{{ old('comment') }}" class="form-control @error('comment') is-invalid @enderror" placeholder="">
                        @error('comment')
                        <span class="text-danger" role="alert">
                               {{ $message }}
                           </span>
                        @enderror
                    </div>
                </div>



                <div class="row mb-3">
                    <div class="form-group col-md-3">
                        <label>Event Category</label>
                        <select name="category_id" class="selectpicker" title="select category" data-live-search="true" data-live-search-placeholder="Search ...">
                            @foreach($event_category as $category)
                                <option value="{{ $category->id }}" {{ old('category_id') == $category->id  ? 'selected' : '' }}>{{ $category->name }}</option>
                            @endforeach
                        </select>
                        @error('category_id')
                        <span class="text-danger" role="alert">
                           {{ $message }}
                       </span>
                        @enderror
                    </div>

                    <div>{{ request('category_id') }}</div>

                    <div class="col-md-3 mt-4 pt-2">
                        <a href="{{ url()->previous() }}" class="btn btn-sm btn-danger">Cancel</a>

                        <button type="submit" class="btn btn-sm btn-primary">Save & Apply</button>
                    </div>

                </div>


                <div class="row mb-4">

                </div>

            </form>

        </div>

    </div> --}}
@endsection
