@extends('layouts.admin')

@section('title', 'Groups')
@section('admin_css')

    <link rel="stylesheet" href="{!! asset('css/dataTables.bootstrap.min.css') !!}" type="text/css">
    <link rel="stylesheet" href="{!! asset('css/dataTables.bootstrap4.css') !!}" type="text/css">

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css"/>

@endsection
@section('content')


    <div class="">

        <div class="card">
            <div class="card-header">
                Groups

                <div class="float-right" data-toggle="modal" data-target="#modal_2">
                    <a class="btn btn-tertiary text-white btn-sm"><i class="fa fa-plus-circle"></i> Add Group</a>
                </div>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                @include('includes.alert')
                <!--begin: Datatable-->
                {!! $dataTable->table(['class'=> 'table table-striped table-bordered',]) !!}
                <!--end: Datatable-->
                </div>
            </div>
        </div>

    </div>






    <!-- Create Event Category Modal -->
    <div class="modal fade position-fixed" id="modal_2" tabindex="-1" role="dialog" aria-labelledby="modal_2" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_title_6">Add Group</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('groups.store') }}">
                    @csrf
                    <div class="modal-body">
                        <div class="row col-md-12">

                            <div class="form-group col-md-12">
                                <label>Group Name</label>
                                <input type="text" name="group" value="{{ old('group') }}" class="form-control @error('group') is-invalid @enderror" required placeholder="">
                                @error('group')
                                <span class="text-danger" role="alert">
                               {{ $message }}
                           </span>
                                @enderror
                            </div>


                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-primary">Save & Apply</button>
                        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="{!! asset('vendor/datatables/buttons.server-side.js') !!}"></script>
    <script src="{!! asset('js/dataTables.buttons.min.js') !!}"></script>
    <script src="{!! asset('js/buttons.server-side.js') !!}"></script>
    {!! $dataTable->scripts() !!}

@endpush



