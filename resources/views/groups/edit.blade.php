@extends('layouts.admin')

@section('title', 'Groups')
@section('content')

    <div class="row">
        <div class="col-lg-5 mx-auto">
            <div class="card">
                <div class="card-header">
                    Edit Group
                </div>
                <div class="card-body">
                    <form method="POST" action="{{route('groups.update', $group->id)}}">
                        @method('PATCH')
                        @csrf
                        <div class="form-group">
                            <label>Group Name</label>
                            <input type="text" name="group" value="{{ old('group') ?? $group->group }}" class="form-control @error('group') is-invalid @enderror">
                            @error('group')
                            <span class="text-danger" role="alert">
                       {{ $message }}
                   </span>
                            @enderror
                        </div>
                        <div class="text-center">
                            <a href="{{ route('groups.index') }}" class="btn btn-sm btn-danger  text-white mr-2"> <i class="fa fa-sm fa-arrow-left"></i> Cancel</a>

                            <button type="submit" class="btn btn-sm btn-primary"> Update </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection
