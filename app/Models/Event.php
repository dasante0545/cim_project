<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    public function members(){
        return $this->belongsToMany(Member::class, 'event_members', 'event_id', 'member_id')->withTimestamps();
    }

    public function category(){
        return $this->belongsTo(EventCategory::class, 'category_id');
    }

    public function creator(){
        return $this->belongsTo(User::class, 'created_by');
    }


}
