<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventCategory extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    public function events(){
        $this->hasMany(Event::class, 'category_id');
    }
}
