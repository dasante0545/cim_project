<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DepartmentMember extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    protected $table = 'department_member';
}
