<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    public function member(){
        $this->hasMany(Member::class, 'group_id');
    }
}
