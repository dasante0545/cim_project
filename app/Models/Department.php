<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    public function members(){
        return $this->belongsToMany(Member::class, DepartmentMember::class, 'department_id','member_id')->withTimestamps();
    }
}
