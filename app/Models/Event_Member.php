<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event_Member extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    protected $table = 'event_members';


}
