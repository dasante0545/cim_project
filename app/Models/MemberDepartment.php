<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberDepartment extends Model
{
    protected $table = 'department_member';
    protected $guarded = ['id'];
    use SoftDeletes;

}
