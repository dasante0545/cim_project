<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    public function events(){
        return $this->belongsToMany(Event::class, 'event_members', 'member_id', 'event_id')->withTimestamps();
    }

    public function departments(){
        return $this->belongsToMany(Department::class, DepartmentMember::class, 'member_id', 'department_id')->withTimestamps();
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function group(){
        return $this->belongsTo(Group::class, 'group_id');
    }
}
