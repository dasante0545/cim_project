<?php

namespace App\Console\Commands;

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ChangeEventStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:event-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change Event Status After Event';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $events = Event::query()->where('status', 'like','pending')->get();

        if ($events->count() > 0){
            $events->map( function ($event) {

                if (Carbon::now()->greaterThan($event->event_date_time)){
                    $event->update(['status' => 'completed']);
                }

            });
        }
    }
}
