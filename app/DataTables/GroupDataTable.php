<?php

namespace App\DataTables;

use App\Models\Group;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class GroupDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at', function ($query){
                return Carbon::parse($query->created_at)->format('D d F, Y g:i A');
            })
            ->editColumn('updated_at', function ($query){
                return Carbon::parse($query->updated_at)->format('D d F, Y g:i A');
            })
            ->addColumn('action', function ($query){
                return '
                        <div class="dropdown">
                            <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
                            </button>
                            <div class="dropdown-menu dropdown-menu-sm">
                                <a class="dropdown-item" href="'. route('groups.edit', $query->id) .'"><i class="fa fa-edit"></i> Edit</a>
                                <a class="dropdown-item" href="'. route('groups.destroy', $query->id) .'" onclick="return confirm(\'Are you sure you want to delete ?\')"><i class="text-danger fas fa-trash-alt"></i> Delete</a>
                            </div>
                        </div>
                        ';
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Group $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Group $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('groupdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('group'),
            Column::make('created_at'),
            Column::make('updated_at'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Group_' . date('YmdHis');
    }
}
