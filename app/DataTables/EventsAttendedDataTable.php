<?php

namespace App\DataTables;

use App\Models\Event;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class EventsAttendedDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('status', static function ($query) {

                $events = json_decode(request()->get('events'));

                if($query->status == 'pending'){
                    return '<a class="badge badge-tertiary badge-lg badge-pill text-uppercase text-center text-white col-md-8"> Event Pending </a>';

                }elseif(in_array($query->id, $events)){
                    return '<a class="badge badge-success badge-lg badge-pill text-uppercase text-center text-white col-md-8"> Attended </a>';

                }else{
                    return '<a class="badge badge-danger badge-lg badge-pill text-uppercase text-center text-white col-md-8"> Didn\'t Attend</a>';
                }
            })
//            ->addColumn('action', 'eventsattendeddatatable.action')
            ->rawColumns(['status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Event $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Event $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('eventsattendeddatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(0)
                    ->buttons(
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('created_at')
                ->visible(false)
                ->exportable(false)
                ->printable(false),
            Column::make('event_name'),
            Column::make('event_date_time'),
            Column::make('status'),
            Column::make('venue'),
//            Column::computed('action')
//                ->exportable(false)
//                ->printable(false)
//                ->width(60)
//                ->addClass('text-center'),

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'EventsAttended_' . date('YmdHis');
    }
}
