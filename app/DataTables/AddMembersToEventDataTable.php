<?php

namespace App\DataTables;

use App\Models\Event;
use App\Models\Member;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class AddMembersToEventDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('select_member', static function ($query) {

                $member =json_decode(request()->get('members'));

                $checked = "checked";
                return '<input type="checkbox" name="member_id[]" value="'.$query->id.'" '. (in_array($query->id, $member) ? $checked : '').' />';
            })
            ->addColumn('status', static function ($query) {

                $member =json_decode(request()->get('members'));
                $event = json_decode(request()->get('event'));

                if($event->status == 'pending'){
                    return '<a class="badge badge-pill badge-lg badge-tertiary text-uppercase text-white col-md-8"> Event Pending </a>';

                }elseif(in_array($query->id, $member)){
                return '<a class="badge badge-pill badge-lg badge-success text-uppercase text-white col-md-8"> Attended </a>';

                }else{
                    return '<a class="badge badge-pill badge-lg badge-danger text-uppercase text-white col-md-8"> Didn\'t Attend</a>';
                }
            })->rawColumns(['select_member', 'status']);
//            ->addColumn('action', 'addmemberstoeventdatatable.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Member $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Member $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('addmemberstoeventdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(3)
                    ->buttons(
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('select_member')->title('#'),
            Column::make('name'),
            Column::make('mobile_number'),
            Column::make('status'),
            Column::make('created_at')->title('Created On'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'AddMembersToEvent_' . date('YmdHis');
    }
}
