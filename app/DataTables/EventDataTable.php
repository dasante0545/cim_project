<?php

namespace App\DataTables;

use App\Models\Event;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class EventDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('event_date', function ($query){
                $event_date = Carbon::parse($query->event_date_time)->format('D d F, Y');
                return $event_date ?? '';
            })
            ->addColumn('event_time', function ($query){
                $event_time = Carbon::parse($query->event_date_time)->format('g:i A');
                return $event_time ?? '';
            })
            ->addColumn('status', function ($query){
                if($query->status == 'pending'){
                    return '<span class="badge badge-danger badge-lg badge-pill text-uppercase text-center text-white btn-block">'. $query->status . '</span>';
                }else{
                    return '<span class="badge badge-success badge-lg badge-pill text-uppercase text-center text-white btn-block">'. $query->status . '</span>';
                }
            })
            ->addColumn('action', function ($query){
                return '
                        <div class="dropdown">
                            <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
                            </button>
                            <div class="dropdown-menu dropdown-menu-sm">
                                <a class="dropdown-item" title="Add members to event" href="'. route('add.members.to.event', ['event' => $query->id]) .'"><i class="fa fa-plus-circle"></i> Add Members</a>
                                <a class="dropdown-item" href="'. route('events.edit', $query->id) .'"><i class="fa fa-edit"></i> Edit</a>
                                <a class="dropdown-item" href="'. route('events.destroy', $query->id) .'" onclick="return confirm(\'Are you sure you want to delete ?\')"><i class="text-danger fas fa-trash-alt"></i> Delete</a>
                            </div>
                        </div>
                        ';
            })
            ->rawColumns(['status', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Event $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Event $model)
    {
        return $model->newQuery()->where('category_id',$this->category_id);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('eventdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(4)
                    ->buttons(
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('event_name'),
            Column::make('event_date'),
            Column::make('event_time'),
            Column::make('status'),
            Column::make('venue'),
            Column::make('comment'),
            Column::make('created_at')->title('Created On'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Event_' . date('YmdHis');
    }
}
