<?php

namespace App\DataTables\DataTables;

use App\Models\Member;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MembersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at', function ($query){
                return Carbon::parse($query->created_at)->format('D d F, Y');
            })
            ->addColumn('action', function ($query){
                return '
<div class="dropdown">
    <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
    </button>
    <div class="dropdown-menu dropdown-menu-sm">
    <a class="dropdown-item" href="'. route('members.events.attended', $query->id) .'"><i class="fa fa-calendar"></i> Events Attended</a>
        <a class="dropdown-item" href="'. route('members.show', $query->id) .'"><i class="fa fa-eye"></i> View</a>
        <a class="dropdown-item" href="'. route('members.edit', $query->id) .'"><i class="fa fa-edit"></i> Edit</a>
        <a class="dropdown-item" href="'. route('members.destroy', $query->id) .'" onclick="return confirm(\'Are you sure you want to delete ?\')"><i class="text-danger fas fa-trash-alt"></i> Delete</a>
    </div>
</div>


                ';
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Member/MembersDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Member $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('datatables-membersdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(5)
                    ->buttons(
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('membership_id'),
            Column::make('name'),
            Column::make('mobile_number'),
            Column::make('residential_address'),
            Column::make('occupation'),
            Column::make('created_at')->title('Created On'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Members_' . date('YmdHis');
    }
}
