<?php

namespace App\Http\Controllers;

use App\DataTables\AddMembersToEventDataTable;
use App\DataTables\EventDataTable;
use App\Models\Event;
use App\Models\Event_Member;
use App\Models\EventCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{

    public function index(EventDataTable $dataTable, Request $request)
    {
        $event_category = EventCategory::query()->find($request->category_id);

        if($event_category){
            return $dataTable->with([
                'category_id' => $request->category_id,
            ])->render('events.index', compact('event_category'));

        }else{
            return redirect()->back()->with('error', 'This Event Category does not exist');
        }

    }


    public function create()
    {
        return view('events.create', [
            'event_category' => EventCategory::all(),
        ]);
    }


    public function store(Request $request)
    {
        $data = $request->validate([
            'event_name' => 'required|string|min:3',
            'event_date_time' => 'required|date_format:Y-m-d\TH:i',
            'venue' => 'required|string|min:3',
            'comment' => 'required|string|min:3',
            'category_id' => 'required|exists:App\Models\EventCategory,id',
        ]);
        $data['created_by'] = auth()->id();
        $category = $request->category_id;

        DB::beginTransaction();
        try {
            Event::query()->create($data);
            DB::commit();
            }catch (\Exception $e){
            DB::rollBack();

            return redirect()->route('events.index',['category_id' =>$category])->with('error', 'Event registration failed');
        }

        return redirect()->route('events.index',['category_id' =>$category])->with('success', 'Event registration was successful');
    }


    public function show(Event $event)
    {
        //
    }


    public function edit(Event $event)
    {
        $event_category = EventCategory::all();
        return view('events.edit', compact('event', 'event_category'));
    }


    public function update(Request $request, Event $event)
    {
//        dd($request->all());

        $data = $request->validate([
            'event_name' => 'required|string|min:3',
            'event_date_time' => 'required|date_format:Y-m-d\TH:i',
            'venue' => 'required|string|min:3',
            'comment' => 'required|string|min:3',
            'category_id' => 'required|exists:App\Models\EventCategory,id',
        ]);
        $data['created_by'] = auth()->id();
        $category = $request->category_id;

        DB::beginTransaction();
        try {
            $event->update($data);
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();

            return redirect()->route('events.index',['category_id' =>$category])->with('error', 'Event update failed');
        }

        return redirect()->route('events.index',['category_id' =>$category])->with('success', 'Event update was successful');
    }


    public function destroy(Event $event)
    {
        DB::beginTransaction();
        try {
            $event->delete();
            DB::commit();
        } catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', 'Event Deletion Failed');

        }

        return back()->with('success', 'Event Deletion was Successful');
    }



    public function AddMembersToEvent(AddMembersToEventDataTable $dataTable, Request $request,Event $event){

        if($request->isMethod("POST")){

            $request->validate([
                'member_id' => 'exists:members,id',
            ]);

//            $event->members()->attach($request->member_id);
            $event->members()->sync($request->member_id);

            $members = $event->members()->pluck('member_id');

            return redirect()->back()->with(['success'=>'Members successfully added to Event',compact('event', 'members')]);
//            return $dataTable->with('success', 'Members successfully added to Event')->render('event_members.create', compact('event', 'members'));

        }

        $members = $event->members()->pluck('member_id');

        return $dataTable->render('event_members.create', compact('event', 'members'));
    }

}
