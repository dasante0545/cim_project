<?php

namespace App\Http\Controllers;

use App\DataTables\DataTables\MembersDataTable;
use App\DataTables\EventsAttendedDataTable;
use App\Models\Department;
use App\Models\Event;
use App\Models\Event_Member;
use App\Models\Group;
use App\Models\Member;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{

    public function index(MembersDataTable $dataTable)
    {
        return $dataTable->render('members.index');
    }

    // All Events Attended by a specific member
    public function EventsAttended(EventsAttendedDataTable $dataTable,Member $member){

        $events = $member->events()->pluck('event_id');

        $finished_events = Event::query()->where('status', 'like', 'completed')->get();

        $count = collect();
        $finished_events->map( function ($event) use($member, $count) {
            $event_attended = Event_Member::query()->where([['event_id','=',"$event->id"],['member_id','=',"$member->id"]])->get();
            if($event_attended->count() > 0){
                $count->push($event_attended);
            }
        });

        $counts = [
            'events_finished' => $finished_events->count(),
            'no_attended' => $count->count(),
            'no_absented' => $finished_events->count() - $count->count()
        ];


        return $dataTable->render('members.events_attended', compact('member', 'events', 'counts'));


    }


    public function create()
    {
        return view('members.create', [
            'groups' => Group::all(),
            'departments' => Department::all(),
        ]);
    }


    public function store(Request $request)
    {
//        dd($request->all());

        $legalAge = Carbon::parse(date('Y-01-01'))->subYear(15)->format('Y-m-d');
        $legalYear = Carbon::parse(date('Y-01-01'))->format('Y');

        $data = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile_number' => 'required|min:10',
            'emergency_contact' => 'nullable|min:10',
            'email' => 'nullable|email',
            'date_of_birth' => "required|date",
            'occupation' => 'nullable',
            'residential_address' => 'nullable|string|min:3',
            'marital_status' => 'required|string|min:3',
            'no_of_children' => 'integer',
            'membership_id' => 'required|integer',
            'date_joined' => 'nullable|before_or_equal:today',
            'year_joined' => "nullable|before_or_equal:$legalYear",
            'group_id' => 'nullable|exists:groups,id',
            'departments' => 'nullable|exists:departments,id',

        ]);

        $membership_id = str_pad($data['membership_id'],5,'0',STR_PAD_LEFT);
        $data['membership_id'] = "CIMBT" . $membership_id;
        $data['user_id'] = auth()->id();
        $data['name'] = $request->first_name. ' '. $request->last_name;
        $data['date_joined'] = $data['year_joined'] ? $data['date_joined'] = null : $data['date_joined'];
        $request['membership_id'] = $data['membership_id'];

        // Check whether registrant is 15 yrs old
        $this->validate($request, $this->validateRules($legalAge), $this->validateMessages());


        DB::beginTransaction();
        try {
            $member = Member::query()->create($data);
            $member->departments()->sync($data['departments']);

            DB::commit();
        } catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', 'Member registration failed');

        }

        return redirect()->route('members.index')->with('success', 'Member registration was successful');


    }


    public function show(Member $member)
    {
        return view('members.show', compact('member'));
    }

    public function edit(Member $member)
    {
        $groups = Group::all();
        $departments = Department::all();
        return view('members.edit', compact('member', 'groups', 'departments'));
    }


    public function update(Request $request, Member $member)
    {
        $legalAge = Carbon::parse(date('Y-01-01'))->subYear(15)->format('Y-m-d');
        $legalYear = Carbon::parse(date('Y-01-01'))->format('Y');

        $data = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile_number' => 'required|min:10',
            'emergency_contact' => 'nullable|min:10',
            'email' => 'nullable|email',
            'date_of_birth' => "required|date",
            'occupation' => 'nullable',
            'residential_address' => 'nullable|string|min:3',
            'marital_status' => 'required|string|min:3',
            'no_of_children' => 'integer',
            'membership_id' => 'required|integer',
            'date_joined' => 'nullable|before_or_equal:today',
            'year_joined' => "nullable|before_or_equal:$legalYear",
            'group_id' => 'nullable|exists:groups,id',
            'departments' => 'nullable|exists:departments,id',
        ]);
        $membership_id = str_pad($data['membership_id'],5,'0',STR_PAD_LEFT);
        $data['membership_id'] = "CIMBT" . $membership_id;
        $data['user_id'] = auth()->id();
        $data['name'] = $request->first_name. ' '. $request->last_name;
        $data['date_joined'] = $data['year_joined'] ? $data['date_joined'] = null : $data['date_joined'];
        $request['membership_id'] = $data['membership_id'];

        // Check whether registrant is 15 yrs old
        $this->validate($request, $this->validateUpdateRules($legalAge, $member->id), $this->validateMessages());

        DB::beginTransaction();
        try {
            $member->update($data);
            $member->departments()->sync($data['departments']);

            DB::commit();
        } catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', 'Member Update failed');

        }

        return redirect()->route('members.index')->with('success', 'Member Update was successful');
    }


    public function destroy(Member $member)
    {
        DB::beginTransaction();
        try {
            $member->delete();
            DB::commit();
        } catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', 'Member Deletion failed');

        }

        return back()->with('success', 'Member Deletion was successful');
    }

    public function validateMessages()
    {
        return [
            'date_of_birth.before' => 'You have to be 15 years old to qualify for registration',
        ];
    }

    public function validateRules($legalAge)
    {
        return [
            'date_of_birth' =>  "before:$legalAge",
            'membership_id' => "unique:members,membership_id",
        ];
    }

    public function validateUpdateRules($legalAge, $member_id)
    {
        return [
            'date_of_birth' =>  "before:$legalAge",
            'membership_id' => "unique:members,membership_id,".$member_id,
        ];
    }
}
