<?php

namespace App\Http\Controllers;

use App\DataTables\DepartmentDataTable;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DepartmentDataTable $dataTable)
    {
        return $dataTable->render('departments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            $data = $request->validate([
                'department' => 'required|min:3',
            ]);
            $data['user_id'] = auth()->id();

            Department::query()->create($data);
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            return back()->with('error', 'Addition of Department Failed! Department Name is required and should have at least 3 characters')->withInput();
        }

        return back()->with('success', 'Addition of Department was successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit(Department $department)
    {
        return view('departments.edit', compact('department'));
    }


    public function update(Request $request, Department $department)
    {
        $data = $request->validate([
            'department' => 'required|min:3',
        ]);
        $data['user_id'] = auth()->id();

        DB::beginTransaction();
        try{
            $department->update($data);
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            return back()->with('error', 'Updating Department Failed!');
        }

        return redirect()->route('departments.index')->with('success', 'Update of Department was successful');

    }


    public function destroy(Department $department)
    {
        DB::beginTransaction();
        try {
            $department->delete();
            DB::commit();
        } catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', 'Department Deletion Failed');

        }

        return back()->with('success', 'Department Deletion was Successful');
    }
}
