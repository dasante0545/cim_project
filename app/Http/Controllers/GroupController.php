<?php

namespace App\Http\Controllers;

use App\DataTables\GroupDataTable;
use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(GroupDataTable $dataTable)
    {
        return $dataTable->render('groups.index');
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            $data = $request->validate([
                'group' => 'required|min:3',
            ]);
            $data['user_id'] = auth()->id();

            Group::query()->create($data);
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            return back()->with('error', 'Addition of Group Failed! Group Name is required and should have at least 3 characters')->withInput();
        }

        return back()->with('success', 'Addition of Group was successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit(Group $group)
    {
        return view('groups.edit', compact('group'));
    }


    public function update(Request $request, Group $group)
    {
        $data = $request->validate([
            'group' => 'required|min:3',
        ]);
        $data['user_id'] = auth()->id();

        DB::beginTransaction();
        try{
            $group->update($data);
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            return back()->with('error', 'Updating Group Failed!');
        }

        return redirect()->route('groups.index')->with('success', 'Update of Group was successful');

    }


    public function destroy(Group $group)
    {
        DB::beginTransaction();
        try {
            $group->delete();
            DB::commit();
        } catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', 'Group Deletion Failed');

        }

        return back()->with('success', 'Group Deletion was Successful');
    }
}
