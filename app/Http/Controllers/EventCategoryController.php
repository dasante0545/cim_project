<?php

namespace App\Http\Controllers;

use App\DataTables\EventCategoryDataTable;
use App\Models\EventCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventCategoryController extends Controller
{

    public function index(EventCategoryDataTable $dataTable)
    {
        return $dataTable->render('event_category.index');
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {

        DB::beginTransaction();
        try{
            $data = $request->validate([
                'name' => 'required|min:5',
            ]);
            $data['user_id'] = auth()->id();

            EventCategory::query()->create($data);
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            return back()->with('error', 'Addition of Event Category Failed! Name is required and should have at least 5 characters')->withInput();
        }

        return back()->with('success', 'Addition of Event Category was successful');
    }


    public function show(EventCategory $eventCategory)
    {
        //
    }


    public function edit(EventCategory $eventCategory)
    {
        return view('event_category.edit', compact('eventCategory'));
    }


    public function update(Request $request, EventCategory $eventCategory)
    {
        $data = $request->validate([
            'name' => 'required|min:5',
        ]);
        $data['user_id'] = auth()->id();

        DB::beginTransaction();
        try{
            $eventCategory->update($data);
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            return back()->with('error', 'Update of Event Category Failed!');
        }

        return redirect()->route('event_categories.index')->with('success', 'Update of Event Category was successful');

    }


    public function destroy(EventCategory $eventCategory)
    {
        DB::beginTransaction();
        try {
            $eventCategory->delete();
            DB::commit();
        } catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', 'Event Category Deletion Failed');

        }

        return back()->with('success', 'Event Category Deletion was Successful');
    }
}
