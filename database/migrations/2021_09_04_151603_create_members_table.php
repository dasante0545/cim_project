<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('membership_id');
            $table->string('name');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('mobile_number');
            $table->string('emergency_contact')->nullable();
            $table->string('email')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->date('date_joined')->nullable();
            $table->string('month_year_joined')->nullable();
            $table->year('year_joined')->nullable();
            $table->string('residential_address')->nullable();
            $table->string('occupation')->nullable();
            $table->string('marital_status')->nullable();
            $table->integer('no_of_children')->nullable();
            $table->integer('group_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function (Blueprint $table){
            $table->dropForeign('members_user_id_foreign');
            $table->dropColumn('user_id');
        });

        Schema::dropIfExists('members');



    }
}
