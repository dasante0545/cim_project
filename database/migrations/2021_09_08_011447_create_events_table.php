<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('category_id');
            $table->string('event_name');
            $table->dateTime('event_date_time');
            $table->string('venue')->nullable();
            $table->string('status')->default('pending');
            $table->string('comment')->nullable();
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('category_id')
                ->references('id')
                ->on('event_categories')
                ->onDelete('cascade');
        });


        Schema::create('event_members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('event_id');
            $table->unsignedBigInteger('member_id');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['event_id', 'member_id']);

            $table->foreign('event_id')
                ->references('id')
                ->on('events')
                ->onDelete('cascade');

            $table->foreign('member_id')
                ->references('id')
                ->on('members')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_members');
        Schema::dropIfExists('events');

    }
}
